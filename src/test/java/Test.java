import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Test {

    @org.junit.Test
    public void testTranslation() throws InterruptedException {
//      System.setProperty("webdriver.chrome.driver", "C:\\Users\\79374\\Downloads\\geckodriver.exe");
        WebDriver driver;
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        driver = new FirefoxDriver(options);
        driver.get("https://translate.google.com/");
        driver.findElement(By.xpath("//*[@id=\"yDmH0d\"]/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div[1]/div[2]/div[3]/c-wiz[1]/span/span/div/textarea")).sendKeys("кошка");
        Thread.sleep(1000);
        boolean result = driver.getPageSource().contains("English");
        Assert.assertTrue("The French sentence translated to '" + "English" + "'", result);
        driver.quit();
    }
}
